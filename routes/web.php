<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

//test route
$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->post('/user', 'UserController@store');

$router->group(['middleware' => 'auth', 'prefix' => 'todo'], function () use ($router) {
   $router->get('/', 'TodoController@index');
   $router->post('/', 'TodoController@store');
   $router->patch('/{todoId}', 'TodoController@update');
   $router->patch('/{todoId}/complete', 'TodoController@complete');
   $router->delete('/{todoId}', 'TodoController@destroy');
});
