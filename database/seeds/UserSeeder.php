<?php

declare(strict_types=1);

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        $now = Carbon::now();

        DB::table('users')
            ->insert([
                [
                    'login' => 'test',
                    'password' => Hash::make('test'),
                    'created_at' => $now,
                    'updated_at' => $now,
                ],
            ]);
    }
}
