<?php

declare(strict_types=1);

use App\Models\Status;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        DB::table('statuses')
            ->insert([
                ['status' => Status::STATUS_TODO],
                ['status' => Status::STATUS_DONE],
            ]);
    }
}
