<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Exceptions\TodoAccessDeniedException;
use App\Services\TodoService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class TodoController extends Controller
{
    private TodoService $todoService;

    public function __construct(TodoService $todoService)
    {
        $this->todoService = $todoService;
    }

    public function index(Request $request): JsonResponse
    {
        return response()->json($this->todoService->list($request->all()));
    }

    public function store(Request $request): JsonResponse
    {
        try {
            $validator = Validator::make(
                $request->all(),
                [
                    'parent_id' => 'nullable|exists:todos,id',
                    'priority' => 'required|numeric|between:1,5',
                    'title' => 'required|max:255',
                    'description' => 'required',
                ]
            );

            if ($validator->fails()) {
                return $this->failedResponse($validator->getMessageBag()->getMessages());
            }

            $todo = $this->todoService->create($request->all());

            return response()->json($todo, Response::HTTP_CREATED);
        } catch (TodoAccessDeniedException $e) {
            return $this->failedResponseFromException($e);
        }
    }

    public function update(Request $request, int $todoId): JsonResponse
    {
        try {
            $todo = $this->todoService->get($todoId);

            $validator = Validator::make(
                $request->all(),
                [
                    'priority' => 'sometimes|required|numeric|between:1,5',
                    'title' => 'sometimes|required|max:255',
                    'description' => 'sometimes|required',
                ]
            );

            if ($validator->fails()) {
                return $this->failedResponse($validator->getMessageBag()->getMessages());
            }

            $todo = $this->todoService->update($todo, $request->all());

            return response()->json($todo);
        } catch (ModelNotFoundException $e) {
            return $this->failedResponseFromException($e, Response::HTTP_NOT_FOUND);
        } catch (TodoAccessDeniedException $e) {
            return $this->failedResponseFromException($e);
        }
    }

    public function complete(int $todoId): JsonResponse
    {
        try {
            $todo = $this->todoService->get($todoId);
            $todo = $this->todoService->complete($todo);

            return response()->json($todo);
        } catch (ModelNotFoundException $e) {
            return $this->failedResponseFromException($e, Response::HTTP_NOT_FOUND);
        } catch (TodoAccessDeniedException $e) {
            return $this->failedResponseFromException($e);
        }
    }

    public function destroy(int $todoId): JsonResponse
    {
        try {
            $todo = $this->todoService->get($todoId);
            $this->todoService->delete($todo);

            return response()->json([], Response::HTTP_NO_CONTENT);
        } catch (ModelNotFoundException $e) {
            return $this->failedResponseFromException($e, Response::HTTP_NOT_FOUND);
        } catch (TodoAccessDeniedException $e) {
            return $this->failedResponseFromException($e);
        }
    }
}
