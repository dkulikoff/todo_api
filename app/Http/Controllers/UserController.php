<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    private User $model;

    public function __construct(User $model)
    {
        $this->model = $model;
    }

    public function store(Request $request): JsonResponse
    {
        $validator = Validator::make(
            $request->all(),
            [
                'login' => 'required|alpha_dash|between:2,255|unique:users',
                'password' => 'required|min:6'
            ]
        );

        if ($validator->fails()) {
            return $this->failedResponse($validator->getMessageBag()->getMessages());
        }

        return response()->json($this->model->create($request->all()), Response::HTTP_CREATED);
    }
}
