<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    protected function failedResponse(array $messages, int $status = Response::HTTP_BAD_REQUEST): JsonResponse
    {
        return response()->json($messages, $status);
    }

    protected function failedResponseFromException(Exception $e, ?int $code = null): JsonResponse
    {
        return $this->failedResponse(
            [$e->getMessage()],
            $code ?: $e->getCode() ?: Response::HTTP_BAD_REQUEST
        );
    }
}
