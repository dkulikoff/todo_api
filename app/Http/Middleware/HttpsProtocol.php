<?php

declare(strict_types=1);

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class HttpsProtocol
{
    /**
     * Handle an incoming request.
     *
     * @param Request  $request
     * @param Closure  $next
     * @param string|null  $guard
     * @return mixed
     */
    public function handle(Request $request, Closure $next, $guard = null)
    {
        if (!$request->isSecure()) {
            return redirect()->to($request->getRequestUri(), Response::HTTP_FOUND, [], true);
        }

        return $next($request);
    }
}
