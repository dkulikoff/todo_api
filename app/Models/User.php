<?php

declare(strict_types=1);

namespace App\Models;

use Carbon\Carbon;
use DateTimeInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;

class User extends Model
{
    protected $hidden = ['password'];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function setPasswordAttribute(string $password): void
    {
        $this->attributes['password'] = Hash::make($password);
    }

    public function create(array $userData): self
    {
        $user = new self();
        $user->login = $userData['login'];
        $user->password = $userData['password'];
        $now = Carbon::now();
        $user->created_at = $now;
        $user->updated_at = $now;
        $user->save();

        return $user;
    }
}
