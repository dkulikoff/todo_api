<?php

declare(strict_types=1);

namespace App\Models;

use DateTimeInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Todo extends Model
{
    protected $hidden = ['parent_id', 'status_id', 'user_id', 'user', 'subTasks'];

    protected $casts = [
        'completed_at' => 'datetime',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function status(): BelongsTo
    {
        return $this->belongsTo(Status::class);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function subTasks(): HasMany
    {
        return $this->hasMany(__CLASS__, 'parent_id')
            ->with('status');
    }

    public function allSubTasks(): HasMany
    {
        return $this->subTasks()->with('allSubTasks');
    }

    public function allPendingSubTasks(): HasMany
    {
        return $this->subTasks()->whereNull('completed_at')->with('allPendingSubTasks');
    }

    public function isComplete(): bool
    {
        return $this->status->isComplete();
    }
}
