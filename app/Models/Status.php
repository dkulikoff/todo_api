<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    public const STATUS_TODO = 'todo';
    public const STATUS_DONE = 'done';

    protected $hidden = ['id'];

    public function scopeTodo(Builder $query): Builder
    {
        return $query->where('status', self::STATUS_TODO);
    }

    public function scopeDone(Builder $query): Builder
    {
        return $query->where('status', self::STATUS_DONE);
    }

    public function isComplete(): bool
    {
        return $this->status === self::STATUS_DONE;
    }
}
