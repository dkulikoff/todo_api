<?php

declare(strict_types=1);

namespace App\Services;

use App\Exceptions\TodoAccessDeniedException;
use App\Models\Status;
use App\Models\Todo;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;

class TodoService
{
    private Todo $model;

    public function __construct(Todo $model)
    {
        $this->model = $model;
    }

    public function list(array $filters): Collection
    {
        $query = $this->model
            ->with(['status', 'allSubTasks'])
            ->where('user_id', Auth::user()->id);

        if (array_key_exists('status', $filters)) {
            $query->whereHas('status', function (Builder $query) use ($filters) {
                return $query->where('status', $filters['status']);
            });
        }

        if (array_key_exists('priority', $filters)) {
            $this->getPriorityWhere($query, is_string($filters['priority']) ?
                [$filters['priority']] : $filters['priority']);
        }

        if (array_key_exists('title', $filters)) {
            $query->whereRaw('MATCH(title) AGAINST (?)', [$filters['title']]);
        }

        if (array_key_exists('order_by', $filters)) {
            $this->getOrderBy($query, $filters['order_by'], $filters['order_direction'] ?? 'asc');
        }

        return $query->get();
    }

    /**
     * @throws ModelNotFoundException
     */
    public function get(int $todoId): Todo
    {
        return $this->model->findOrFail($todoId);
    }

    /**
     * @throws TodoAccessDeniedException
     */
    public function create(array $todoData): Todo
    {
        $parentId = $todoData['parent_id'] ? (int)$todoData['parent_id'] : null;

        if ($parentId && $this->get($parentId)->user_id !== Auth::user()->id) {
            throw new TodoAccessDeniedException(
                'You are not allowed to create sub tasks for tasks created by other users.'
            );
        }

        $todo = new Todo();
        $todo->parent_id = $parentId;
        $todo->priority = $todoData['priority'];
        $todo->title = $todoData['title'];
        $todo->description = $todoData['description'];
        $todo->status()->associate(Status::todo()->first());
        $todo->user()->associate(Auth::user());
        $now = Carbon::now();
        $todo->created_at = $now;
        $todo->updated_at = $now;
        $todo->save();

        return $todo;
    }

    /**
     * @throws TodoAccessDeniedException
     */
    public function update(Todo $todo, array $todoData): Todo
    {
        $this->isTodoAccessible($todo);
        $todo->priority = $todoData['priority'] ?? $todo->priority;
        $todo->title = $todoData['title'] ?? $todo->title;
        $todo->description = $todoData['description'] ?? $todo->description;
        $todo->save();

        return $todo;
    }

    /**
     * @throws TodoAccessDeniedException
     */
    public function complete(Todo $todo): Todo
    {
        $this->isTodoAccessible($todo);
        if ($todo->isComplete()) {
            return $todo;
        }

        if ($todo->allPendingSubTasks()->exists()) {
            throw new TodoAccessDeniedException('You can not complete a todo task with pending subtasks.');
        }

        $todo->status()->associate(Status::done()->first());
        $todo->completed_at = Carbon::now();
        $todo->save();

        return $todo;
    }

    /**
     * @throws TodoAccessDeniedException
     */
    public function delete(Todo $todo): void
    {
        $this->isTodoAccessible($todo);
        if ($todo->isComplete()) {
            throw new TodoAccessDeniedException('You can not delete completed todo task.');
        }

        if ($todo->subTasks()->count() > 0) {
            throw new TodoAccessDeniedException('You can not delete a todo task with subtasks.');
        }

        $todo->delete();
    }

    /**
     * @throws TodoAccessDeniedException
     */
    private function isTodoAccessible(Todo $todo): void
    {
        if ($todo->user->id !== Auth::user()->id) {
            throw new TodoAccessDeniedException();
        }
    }

    private function getOrderBy(Builder $query, string $orderBy, string $direction): Builder
    {
        if (!Schema::hasColumn($this->model->getTable(), $orderBy)) {
            return $query;
        }

        if (strtolower($direction) === 'desc') {
            return $query->orderByDesc($orderBy);
        }

        return $query->orderBy($orderBy);
    }

    private function getPriorityWhere(Builder $query, array $filters): Builder
    {
        foreach ($filters as $filter) {
            if (Str::contains($filter, ':')) {
                $sign = $this->getComparisonSign(Str::before($filter, ':'));

                if ($sign) {
                    $query->where('priority', $sign, trim(Str::after($filter, ':')));
                    continue;
                }
            }

            $query->where('priority', $filter);
        }

        return $query;
    }

    private function getComparisonSign(string $sign): ?string
    {
        $signs = [
            'lt' => '<',
            'lte' => '<=',
            'gt' => '>',
            'gte' => '>=',
        ];

        return $signs[trim($sign)] ?? null;
    }
}
