<?php

declare(strict_types=1);

namespace App\Exceptions;

use Exception;
use Illuminate\Http\Response;

class TodoAccessDeniedException extends Exception
{
    public function __construct(string $message = 'You are not allowed to change the records created by other users.')
    {
        parent::__construct($message, Response::HTTP_FORBIDDEN);
    }
}
